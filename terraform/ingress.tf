resource "kubernetes_ingress" "petclinic-ingress" {
  metadata {
    name = var.ingress_name
  }
  spec {
    tls {
      hosts = [var.host_name]
      secret_name = var.secret_name
    }
    rule {
      host = var.host_name
      http {
        path {
          path = "/"
          backend {
            service_name = var.service_name
            service_port = "80"
          }
        }
      }
    }
  }
}