resource "kubernetes_deployment" "petclinic-deployment" {
  metadata {
    name = var.deployment_name
  }

  spec {
    replicas = var.deployment_count

    selector {
      match_labels = {
        app = "petclinic"
      }
    }

    template {
      metadata {
        labels = {
          app = "petclinic"
        }
      }
      spec {
        container {
          image = var.image_name
          name = var.container_name
          port{
            container_port = var.container_port
          }
          env {
            name = "MYSQL_USER"
            value_from{
              secret_key_ref{
                name = "petclinic"
                key = "petclinic-username"
              }
            } 
          }
          env {
            name = "MYSQL_PASS"
            value_from{
              secret_key_ref{
                name = "petclinic"
                key = "petclinic-password"
              }
            }
          }
          env {
            name = "MYSQL_URL"
            value_from{
              secret_key_ref{
                name = "petclinic"
                key = "petclinic-url"
              }
            }
          }
        }
      }
    }
  }
}
