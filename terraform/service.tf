resource "kubernetes_service" "petclinic-service"{
  metadata {
    name = var.service_name
  }
  spec {
    selector = {
      app = "petclinic"
    }

    port {
      protocol = "TCP"
      port = 80
      target_port = "8080"
    }
  }
}