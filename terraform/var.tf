variable "deployment_name" {
  type = string
}

variable "deployment_count" {
  type = string
}

variable "image_name" {
  type = string
}

variable "container_name" {
  type = string
}

variable "container_port" {
  type = string
}

variable "service_name" {
  type = string
}

variable "ingress_name" {
  type = string
}

variable "host_name" {
  type = string
}

variable "secret_name" {
  type = string
}
