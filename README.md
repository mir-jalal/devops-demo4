# DEVOPS-DEMO-3
___

This is Demo-3 task repo for IbaTech Academy

> You can check my gitlab repo for automation of creation of k8s cluster on-premise
> [K8S-Automation](https://gitlab.com/mir.jalal/k8s-cluster-automation)
___

### To-Do-List

- Subtask I - Create [Terraform](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_cluster) scripts for GKE(more prefered)/AWS EKS infrasrtucture:

   * [x] Create [GCP account](https://console.cloud.google.com/) / or AWS
   * [x] Install kubectl + terraform + gcloud SDK
   * [x] Deploy and access Kubernetes Dashboard 
   * [x] Deploy mysql + application on GKE/AWS EKS using pods
   * [x] Create service to access your application from external networks

- Subtask II - Use Jenkins/Gitlab for running terraform for spawn GKE/EKS

    * [x] Setup Gitlab account/Gitlab-CI or Jenkins.
    * [x] Create a job that will be triggered on changes in master branch in gitlab repo. It should build your project and create jar package.
    * [x] Create a 2nd Job and use your Terraform scripts for infrastructure deployment.
    * [x] Create a step to check if the application is up and running(health check).

- Additional tasks

    * [x] If you want you can use [Cloud SQL](https://cloud.google.com/sql) Instead pod
    * [x] You could use K8S on-premise
